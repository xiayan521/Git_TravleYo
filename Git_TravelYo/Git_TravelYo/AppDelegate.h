//
//  AppDelegate.h
//  Git_TravelYo
//
//  Created by lanou3g on 15/11/5.
//  Copyright © 2015年 TravelYo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

